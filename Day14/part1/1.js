// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let polymerTemplate;
let outputString = "";
const rules = new Map();
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  } else if (!polymerTemplate) {
    polymerTemplate = line;
    return;
  } else if (line.length === 0) {
    return;
  }

  const [key, value] = line.split(" -> ");
  rules.set(key, value);
};

const applyRules = (template) => {
  const newTeplateArray = [];
  for (let i = 0; i < template.length - 1; i++) {
    const key = template[i] + template[i + 1];
    const value = rules.get(key);
    newTeplateArray.push(template[i], value);
  }
  newTeplateArray.push(template[template.length - 1]);
  return newTeplateArray;
};

const processPolymer = () => {
  for (let i = 0; i < 10; i++) {
    const newTemplateArray = applyRules(polymerTemplate);
    polymerTemplate = newTemplateArray.join("");
    console.log(`step ${i + 1}: ${polymerTemplate}`);
    outputString += `step ${i + 1}: ${polymerTemplate}\n`;
  }
};

const findCommonElements = () => {
  const counts = new Map();
  for (let i = 0; i < polymerTemplate.length; i++) {
    counts.set(polymerTemplate[i], (counts.get(polymerTemplate[i]) || 0) + 1);
  }
  let min;
  let max;
  console.log("...");
  for (let [key, value] of counts.entries()) {
    if (!min || value < min) {
      min = value;
    }
    if (!max || value > max) {
      max = value;
    }
  }
  console.log({ min, max, substract: max - min });
  outputString += `min: ${min}, max: ${max}, substract: ${max - min}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processPolymer();
  findCommonElements();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
