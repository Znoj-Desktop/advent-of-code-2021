// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let polymerTemplate;
let outputString = "";
const rules = new Map();
let polymerMatrix;

let key;
let value;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  } else if (!polymerTemplate) {
    polymerTemplate = line.split("");
    polymerMatrix = new Map();
    for (let i = 0; i < polymerTemplate.length - 1; i++) {
      let key = polymerTemplate[i] + polymerTemplate[i + 1];
      saveKey(polymerMatrix, key);
    }
    return;
  } else if (line.length === 0) {
    return;
  }

  [key, value] = line.split(" -> ");
  rules.set(key, value);
};

const saveKey = (matrix, key, value = 1) => {
  matrix.set(key, (matrix.get(key) || 0) + value);
};

const processPolymer = () => {
  for (let step = 1; step <= 40; step++) {
    let newPolymerMatrix = new Map();
    for (let [key, value] of polymerMatrix.entries()) {
      let ruleValue = rules.get(key);
      saveKey(newPolymerMatrix, key[0] + ruleValue, value);
      saveKey(newPolymerMatrix, ruleValue + key[1], value);
    }
    polymerMatrix = new Map(newPolymerMatrix);
  }
};

const findCommonElements = () => {
  const counts = new Map();
  for (let [key, value] of polymerMatrix.entries()) {
    // every character is there twice except for a 1st and the last one (will be fixed)
    saveKey(counts, key[0], value / 2);
    saveKey(counts, key[1], value / 2);
  }
  //fix for the first and last value
  saveKey(counts, polymerTemplate[0], 0.5);
  saveKey(counts, polymerTemplate[polymerTemplate.length - 1], 0.5);

  let min;
  let max;
  for (let [key, value] of counts.entries()) {
    if (!min || value < min) {
      min = value;
    }
    if (!max || value > max) {
      max = value;
    }
  }
  console.log({ counts, min, max, substract: max - min });
  outputString += `min: ${min}, max: ${max}, substract: ${max - min}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processPolymer();
  findCommonElements();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
