// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";
let window = [];
let previous;
let count = 0;
let increases = 0;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }
  let m = count % 3;
  window[m] = +line;
  let sum = 0;
  if (count > 2) {
    sum = window[0] + window[1] + window[2];
  }
  if (previous < sum) {
    increases++;
  }
  previous = sum;

  console.log(`Case #${caseIndex++}: ${line} prev: ${sum} inc: ${increases}`);
  outputString += `increases - ${increases}\n`;
  count++;
  // clear
};
//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
