// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let uniqueDigits = 0;
let bigSum = 0;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let input = line
    .split(" | ")[0]
    .split(" ")
    .sort((a, b) => a.length - b.length);
  let resultNumbers = line.split(" | ")[1].split(" ");
  let numbers = [];

  for (let s of input) {
    if (s.length === 2) {
      // 1
      numbers[1] = s;
    } else if (s.length === 3) {
      // 7
      numbers[7] = s;
    } else if (s.length === 4) {
      // 4
      numbers[4] = s;
    } else if (s.length === 5) {
      // 3
      let is3 = true;
      for (let c of numbers[1]) {
        if (!s.includes(c)) {
          is3 = false;
          break;
        }
      }
      if (is3) {
        numbers[3] = s;
        continue;
      }
    } else if (s.length === 6) {
      // 6, 9, 0
      let is6 = false;
      for (let c of numbers[1]) {
        if (!s.includes(c)) {
          is6 = true;
          break;
        }
      }
      if (is6) {
        numbers[6] = s;
        continue;
      }

      let is9 = true;
      for (let c of numbers[3]) {
        if (!s.includes(c)) {
          is9 = false;
          break;
        }
      }
      if (is9) {
        numbers[9] = s;
        continue;
      }
      numbers[0] = s;
    } else if (s.length === 7) {
      // 8
      numbers[8] = s;
    }
  }

  for (let s of input) {
    if (s.length === 5 && s !== numbers[3]) {
      // 2, 5
      let is5 = true;
      for (let c of s) {
        if (!numbers[6].includes(c)) {
          is5 = false;
          break;
        }
      }
      if (is5) {
        numbers[5] = s;
        continue;
      }
      numbers[2] = s;
    }
  }
  for (n in numbers) {
    numbers[n] = numbers[n].split("").sort().join("");
  }

  let result = [];
  for (n in resultNumbers) {
    resultNumbers[n] = resultNumbers[n].split("").sort().join("");
    result.push(numbers.indexOf(resultNumbers[n]));
  }

  bigSum += +result.join("");
  console.log("done: ", bigSum);

  outputString += `${bigSum}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
