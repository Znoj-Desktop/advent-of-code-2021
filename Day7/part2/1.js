// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";

//#endregion
const getFuel = (arr, val) => {
  fuel = 0;
  for (a of arr) {
    let length = Math.abs(a - val);
    fuel += (length + 1) * (length / 2);
  }
  return fuel;
};
//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let state = line
    .split(",")
    .map((l) => +l)
    .sort((a, b) => +a - b);
  let avg = Math.round(state.reduce((a, b) => +a + b, []) / state.length);

  let minIndex = avg;
  let minValue = getFuel(state, minIndex);

  let prevLeft = minValue;
  let prevRight = minValue;
  let doLeft = true;
  let doRight = true;

  for (let i = 1; i < state.length / 2; i++) {
    if (doLeft) {
      let newLeft = getFuel(state, avg - i);
      if (newLeft < minValue) {
        minValue = newLeft;
        minIndex = avg - i;
      } else if (newLeft > prevLeft) {
        doLeft = false;
      }
      prevLeft = newLeft;
    }
    if (doRight) {
      let newRight = getFuel(state, avg + i);
      if (newRight < minValue) {
        minValue = newRight;
        minIndex = avg + i;
      } else if (newRight > prevRight) {
        doRight = false;
      }
      prevRight = newRight;
    }
  }
  console.log("done: " + minIndex, minValue);

  outputString += `${minValue}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
