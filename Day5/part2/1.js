// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";

let inputNumbers = [];
let matrix = [];
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  const [s, e] = line.split(" -> ");
  const start = s.split(",").map((l) => +l);
  const end = e.split(",").map((l) => +l);

  if (start[0] === end[0]) {
    if (start[1] > end[1]) {
      min = end[1];
      max = start[1];
    } else {
      min = start[1];
      max = end[1];
    }
    for (let i = min; i <= max; i++) {
      if (!matrix[i]) {
        matrix[i] = [];
      }
      if (matrix[i][start[0]]) {
        matrix[i][start[0]]++;
      } else {
        matrix[i][start[0]] = 1;
      }
    }
  } else if (start[1] === end[1]) {
    if (!matrix[start[1]]) {
      matrix[start[1]] = [];
    }
    if (start[0] > end[0]) {
      min = end[0];
      max = start[0];
    } else {
      min = start[0];
      max = end[0];
    }
    for (let i = min; i <= max; i++) {
      if (matrix[start[1]][i]) {
        matrix[start[1]][i]++;
      } else {
        matrix[start[1]][i] = 1;
      }
    }
  } else {
    if (start[0] < end[0]) {
      if (start[1] < end[1]) {
        for (let y = start[0], x = start[1]; y <= end[0]; y++, x++) {
          if (!matrix[x]) {
            matrix[x] = [];
          }
          if (matrix[x][y]) {
            matrix[x][y]++;
          } else {
            matrix[x][y] = 1;
          }
        }
      } else {
        for (let y = start[0], x = start[1]; y <= end[0]; y++, x--) {
          if (!matrix[x]) {
            matrix[x] = [];
          }
          if (matrix[x][y]) {
            matrix[x][y]++;
          } else {
            matrix[x][y] = 1;
          }
        }
      }
    } else {
      if (start[1] < end[1]) {
        for (let y = start[0], x = start[1]; y >= end[0]; y--, x++) {
          if (!matrix[x]) {
            matrix[x] = [];
          }
          if (matrix[x][y]) {
            matrix[x][y]++;
          } else {
            matrix[x][y] = 1;
          }
        }
      } else {
        for (let y = start[0], x = start[1]; y >= end[0]; y--, x--) {
          if (!matrix[x]) {
            matrix[x] = [];
          }
          if (matrix[x][y]) {
            matrix[x][y]++;
          } else {
            matrix[x][y] = 1;
          }
        }
      }
    }
  }
  let sum = 0;
  for (row of matrix) {
    for (y in row) {
      if (row[y] > 1) {
        sum++;
      }
    }
  }

  console.log("done: " + sum);

  outputString += `${sum}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
