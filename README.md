# Advent of Code 2021

### **Description**

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language

---

### **Technology**

JavaScript, NodeJS

---

### **Year**

2021

---

### **Structure**

  - `1.in` // input file
  - `1.js` // source file where input/stdin is processed
  - `1.out` // output file

I use template.js which I created

- comment input and uncomment process.stdin for production

1. option to run script (with debugger)

- with extension https://marketplace.visualstudio.com/items?itemName=miramac.vscode-exec-node&ssr=false#overview
- run it with F8 (focus in file), stop it with F9
- for debbugger set in VS Code in Settings > `node debug` > Auto Attach: On

2. option with hot deploy:

- `npm i -g nodemon`
- `nodemon --inspect 1.js`

---

### **Screenshots**

![](README/main.png)
