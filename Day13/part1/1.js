// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
const matrix = [];
let finalPaths = [];
let xMax = 0;
let folded = false;
//#endregion

const foldUp = (by) => {
  for (let y = 1; y < matrix.length - by; y++) {
    for (let x = 0; x <= xMax; x++) {
      if (matrix[by + y][x] === "#") {
        matrix[by - y][x] = "#";
        matrix[by + y][x] = ".";
      }
    }
  }
  matrix.length = by;
};

const foldLeft = (by) => {
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 1; x <= xMax - by; x++) {
      if (matrix[y][by + x] === "#") {
        matrix[y][by - x] = "#";
        matrix[y][by + x] = ".";
      }
    }
  }

  for (let y = 0; y < matrix.length; y++) {
    matrix[y].length = by;
    xMax = by - 1;
  }
};

const lineUpAndCount = () => {
  let count = 0;
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x <= xMax; x++) {
      if (!matrix[y]) {
        matrix[y] = [];
      }
      if (!matrix[y][x]) {
        matrix[y][x] = ".";
      } else if (matrix[y][x] === "#") {
        count++;
      }
    }
  }
  return count;
};
//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let split = line.split(",");
  let x = +split[0];
  let y = +split[1];
  if (isNaN(y)) {
    let split = line.split("=");
    let fold = split[0];
    let by = +split[1];
    if (!folded) {
      if (isNaN(by)) {
        return;
      } else if (fold.includes("y")) {
        foldUp(by);
      } else {
        foldLeft(by);
        folded = true;
      }
    } else {
      return;
    }
    const count = lineUpAndCount();
    for (let row = 0; row < matrix.length; row++) {
      outputString += `${matrix[row]}\n`;
    }
    outputString += `count: ${count}\n\n`;
    console.log(matrix, count);
    return;
  }

  if (+x > xMax) {
    xMax = +x;
  }

  if (!matrix[y]) {
    matrix[y] = [];
  }

  matrix[y][x] = "#";
  lineUpAndCount();
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
