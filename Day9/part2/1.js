// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let matIndex = 0;
const matrix = [];
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }
  matrix[matIndex++] = line.split("").map((l) => +l);
};

const getBasin = (x, y) => {
  if (
    x < 0 ||
    y < 0 ||
    x > matrix.length - 1 ||
    y > matrix[0].length - 1 ||
    matrix[x][y] === 9
  ) {
    return 0;
  }
  matrix[x][y] = 9;
  let sum = 1;
  sum += getBasin(x + 1, y);
  sum += getBasin(x - 1, y);
  sum += getBasin(x, y + 1);
  sum += getBasin(x, y - 1);
  return sum;
};

const processMatrix = () => {
  let basins = [];
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let point = matrix[i][j];
      if (point < 9) {
        basins.push(getBasin(i, j));
      }
    }
  }

  let result = basins
    .sort((a, b) => -a + b)
    .slice(0, 3)
    .reduce((sum, el) => (sum *= el), 1);

  console.log("done: ", result);

  outputString += `${result}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processMatrix();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
