// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let matIndex = 0;
const matrix = [];
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }
  matrix[matIndex++] = line.split("").map((l) => +l);
};

const processMatrix = () => {
  let riskLevel = 0;
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let point = matrix[i][j];
      if (i > 0 && matrix[i - 1][j] <= point) {
        continue;
      }
      if (i < matrix.length - 1 && matrix[i + 1][j] <= point) {
        continue;
      }
      if (j > 0 && matrix[i][j - 1] <= point) {
        continue;
      }
      if (j < matrix[i].length - 1 && matrix[i][j + 1] <= point) {
        continue;
      }
      riskLevel += point + 1;
    }
  }

  console.log("done: ", riskLevel);

  outputString += `${riskLevel}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processMatrix();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
