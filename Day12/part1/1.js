// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
const matrix = new Map();
let finalPaths = [];
//#endregion

const saveValue = (key, value) => {
  if (matrix.has(key)) {
    matrix.get(key).add(value);
  } else {
    matrix.set(key, new Set([value]));
  }
};

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let input = line.split("-");
  saveValue(...input);
  saveValue(input[1], input[0]);
};

const getPath = (mapKey, path) => {
  if (mapKey == "end") {
    // console.log([...path, 'end']);
    return [...path, "end"];
  }
  for (let item of matrix.get(mapKey)) {
    if (item === item.toLowerCase() && path.includes(item)) {
      continue;
    }
    let newPath = getPath(item, [...path, mapKey]);
    newPath && newPath.length > 0 && finalPaths.push(newPath);

    // console.log(finalPaths);
  }
};

const getPaths = () => {
  getPath("start", []);
  console.log(finalPaths.length);
  outputString += `${finalPaths.length}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  getPaths();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
