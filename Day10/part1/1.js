// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let matIndex = 0;
const matrix = [];
const map = new Map();
map.set("{", "}");
map.set("[", "]");
map.set("<", ">");
map.set("(", ")");
const scoreMap = new Map([
  [")", 3],
  ["]", 57],
  ["}", 1197],
  [">", 25137],
]);
let score = 0;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let input = line.split("");
  let controll = [];

  for (el of input) {
    if (Array.from(map.keys()).indexOf(el) >= 0) {
      controll.push(el);
    } else {
      const popEl = controll.pop();
      if (map.get(popEl) !== el) {
        console.log(`Expected ${map.get(popEl)}, but found ${el} instead`);
        score += scoreMap.get(el);
        break;
      }
    }
  }

  console.log(score);

  outputString += `${score}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
