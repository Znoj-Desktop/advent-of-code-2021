// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";

//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let state = line.split(",").map((l) => +l);

  for (let i = 0; i < 80; i++) {
    let newFish = 0;
    state = state.map((fish) => {
      if (fish === 0) {
        newFish++;
        return 6;
      }
      return --fish;
    });
    for (let f = 0; f < newFish; f++) {
      state.push(8);
    }
    // console.log({state})
  }

  console.log("done: " + state.length);

  outputString += `${state.length}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
