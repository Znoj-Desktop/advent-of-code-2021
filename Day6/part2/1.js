// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";

//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let state = line.split(",").map((l) => +l);
  let stateArr = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (el of state) {
    console.log({ el });
    stateArr[el]++;
  }
  console.log({ stateArr });

  for (let i = 0; i < 256; i++) {
    newFishes = stateArr[0];
    for (let i = 1; i < 9; i++) {
      stateArr[i - 1] = stateArr[i];
    }
    stateArr[6] += newFishes;
    stateArr[8] = newFishes;
    console.log({ stateArr });
  }

  console.log("done: " + stateArr.reduce((x, y) => +x + y, []));

  outputString += `${state.length}\n`;
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
