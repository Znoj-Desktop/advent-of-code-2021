// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";

let inputNumbers = [];
let matrixes = [];
let matIndex = -1;
let lineIndex = -1;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  if (inputNumbers.length === 0) {
    inputNumbers = line.split(",").map((l) => +l);
    return;
  }

  if (line.length < 1) {
    matIndex++;
    lineIndex = 0;
    return;
  }
  if (!matrixes[matIndex]) {
    matrixes[matIndex] = [];
  }
  matrixes[matIndex][lineIndex++] = line
    .split(/ +/)
    .filter((l) => l.length > 0)
    .map((l) => +l);
};

const createFalseMatrix = (dim) => {
  let mat = [];
  for (let i = 0; i < dim; i++) {
    for (let j = 0; j < dim; j++) {
      if (!mat[i]) {
        mat[i] = [];
      }
      mat[i][j] = false;
    }
  }
  return mat;
};

const processMatrix = () => {
  let matrixCheck = [];
  let i = 0;
  for (mat of matrixes) {
    matrixCheck[i] = createFalseMatrix(5);
    i++;
  }

  let winners = new Set();
  let winnerMatrix = -1;
  out: for (i = 0; i < inputNumbers.length; i++) {
    for (let m = 0; m < matrixes.length; m++) {
      for (let x = 0; x < matrixes[m].length; x++) {
        for (let y = 0; y < matrixes[m].length; y++) {
          if (+matrixes[m][x][y] === +inputNumbers[i]) {
            matrixCheck[m][x][y] = true;
          }
        }
      }
    }

    for (let m = 0; m < matrixCheck.length; m++) {
      for (let x = 0; x < matrixCheck[m].length; x++) {
        for (let y = 0; y < matrixCheck[m].length; y++) {
          if (!matrixCheck[m][x][y]) {
            break;
          }
          if (y === matrixCheck[m].length - 1) {
            winners.add(m);
            if (winners.size === matrixes.length) {
              winnerMatrix = m;
              break out;
            }
          }
        }
      }
    }

    for (let m = 0; m < matrixCheck.length; m++) {
      for (let x = 0; x < matrixCheck[m].length; x++) {
        for (let y = 0; y < matrixCheck[m].length; y++) {
          if (!matrixCheck[m][y][x]) {
            break;
          }
          if (y === matrixCheck[m].length - 1) {
            winners.add(m);
            if (winners.size === matrixes.length) {
              winnerMatrix = m;
              break out;
            }
          }
        }
      }
    }
  }

  let sum = 0;
  for (let x = 0; x < matrixCheck[winnerMatrix].length; x++) {
    for (let y = 0; y < matrixCheck[winnerMatrix].length; y++) {
      !matrixCheck[winnerMatrix][x][y] && (sum += matrixes[winnerMatrix][x][y]);
    }
  }

  console.log("done: " + sum * inputNumbers[i]);

  outputString += `${sum * inputNumbers[i]}\n`;
};
//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processMatrix();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
