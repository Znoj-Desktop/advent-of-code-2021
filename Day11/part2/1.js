// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let matIndex = 0;
const matrix = [];
let flashes = 0;
let allFlashed;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let input = line.split("");
  matrix[matIndex++] = input;

  // console.log(matrix);
  // outputString += `${matrix}\n`;
};

const light = (row, col) => {
  if (row < 0 || col < 0 || row >= matrix.length || col >= matrix[0].length) {
    return;
  }
  if (matrix[row][col] == -1) {
  } else if (matrix[row][col] < 9) {
    matrix[row][col]++;
  } else {
    matrix[row][col] = -1;
    lightNeighbours(row, col);
  }
};

const lightNeighbours = (row, col) => {
  light(row - 1, col - 1);
  light(row - 1, col);
  light(row - 1, col + 1);
  light(row, col - 1);
  light(row, col + 1);
  light(row + 1, col - 1);
  light(row + 1, col);
  light(row + 1, col + 1);
};

const clearFlashed = () => {
  allFlashed = true;
  for (let row = 0; row < matrix.length; row++) {
    for (let col = 0; col < matrix[0].length; col++) {
      if (matrix[row][col] == -1) {
        matrix[row][col] = 0;
        flashes++;
      } else {
        allFlashed = false;
      }
    }
  }
};

const step = (count) => {
  for (let row = 0; row < matrix.length; row++) {
    for (let col = 0; col < matrix[0].length; col++) {
      light(row, col);
    }
  }
  clearFlashed();
  if (allFlashed) {
    console.log("all flashed", count);
    outputString += `all flashed in step ${count}\n`;
  }
  // console.log(matrix);
};

const doFlashes = () => {
  for (let i = 1; i < 10000; i++) {
    step(i);
    if (allFlashed) {
      break;
    }
  }
};

//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  doFlashes();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
