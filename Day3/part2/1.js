// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let TestCases;
let outputString = "";
let lines = [];
let ones;
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }
  lines.push(line);
};

const processing = () => {
  let oxygen = [];
  let co2 = [];

  ones = 0;
  for (const line of lines) {
    ones += +line[0];
  }
  let main = ones >= lines.length / 2 ? 1 : 0;
  for (const line of lines) {
    if (+line[0] === main) {
      oxygen.push(line);
    } else {
      co2.push(line);
    }
  }
  let i = 1;
  while (oxygen.length > 1) {
    ones = 0;
    for (const line of oxygen) {
      ones += +line[i];
    }
    main = ones >= oxygen.length / 2 ? 1 : 0;
    oxygen = oxygen.filter((line) => +line[i] === main);
    i++;
  }

  i = 1;
  while (co2.length > 1) {
    ones = 0;
    for (const line of co2) {
      ones += +line[i];
    }
    main = ones >= co2.length / 2 ? 0 : 1;
    co2 = co2.filter((line) => +line[i] === main);
    i++;
  }

  let ox = parseInt(oxygen[0], 2);
  let co = parseInt(co2[0], 2);
  let power = ox * co;

  console.log(`
  ox: ${oxygen},
  co2: ${co2},
  ${power}`);
  outputString += `${power}\n`;
  // clear
};
//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  processing();
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
