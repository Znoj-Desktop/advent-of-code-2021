// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "2";
let caseIndex = 1;
let TestCases;
let outputString = "";
let ones = [];
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  }

  let i = 0;
  for (const el of line) {
    if (!ones[i]) {
      ones[i] = 0;
    }
    ones[i] += +el;
    i++;
  }

  i = 0;
  let gammaArr = [];
  let epsilonArr = [];
  for (const one of ones) {
    gammaArr[i] = one > caseIndex / 2 ? 1 : 0;
    epsilonArr[i] = (gammaArr[i] + 1) % 2;
    i++;
  }

  let gamma = parseInt(gammaArr.join(""), 2);
  let epsilon = parseInt(epsilonArr.join(""), 2);
  let power = gamma * epsilon;

  console.log(
    `Case #${caseIndex++}: ${line}, ${ones}, ${gamma}, ${epsilon}, ${power}`
  );
  outputString += `${power}\n`;
  // clear
};
//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion
